package com.imocc.hadoop.project;/**
 * Created by Administrator on 2019/2/22 0022.
 */

import com.kumkee.userAgent.UserAgent;
import com.kumkee.userAgent.UserAgentParser;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ydf
 * @com kt
 * @create 2019-02-22 下午 12:00
 **/
public class UserAgentTest {

    @Test
    public void testReadFile() throws IOException {
        String path = "F:\\nginx\\nginx-1.14.0\\logs\\tt";
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)))
        );
        UserAgentParser userAgentParser = new UserAgentParser();
        String line = "";
        Map<String ,Integer> browserMap=new HashMap<String,Integer>();
        while (line != null) {
            line = reader.readLine();
            if (StringUtils.isNotBlank(line)) {
                String source = line.substring(getCharacterPostion(line, "\"", 5)) + 1;
                UserAgent agent = userAgentParser.parse(source);
                String brower = agent.getBrowser();
                String engine = agent.getEngine();
                String engineVersion = agent.getEngineVersion();
                String os = agent.getOs();
                String platform = agent.getPlatform();
                boolean isMoblie = agent.isMobile();
                Integer browserValue=browserMap.get(brower);
                if(browserValue !=null){
                    browserMap.put(brower,browserValue+1);
                }else {
                    browserMap.put(brower,1);
                }
                System.out.println(brower + "," + engine + "," + engineVersion + "," + os + "," + platform + "," + isMoblie);
            }
        }
        System.out.println("--------------------------------");
        for(Map.Entry<String,Integer> entry:browserMap.entrySet()){
            System.out.println(entry.getKey()+":"+entry.getValue());
        }
    }

    @Test
    public void testGetCharacterPostion() {
        String value = "10.1.0.5 - - [22/Feb/2019:11:08:37 +0800] \"GET /img/collaborators/glasgow.jpg HTTP/1.1\" 200 20141 \"http://10.1.0.52/\" \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36\" \"-\"";
        int index = getCharacterPostion(value, "\"", 5);
        System.out.println(index);
    }

    /**
     * 获取指定字符串中指定标识的字符串出现的索引位置
     *
     * @param value
     * @param operator
     * @param index
     * @return
     */
    private int getCharacterPostion(String value, String operator, int index) {
        Matcher slashMatcher = Pattern.compile(operator).matcher(value);
        int mIdx = 0;
        while (slashMatcher.find()) {
            mIdx++;
            if (mIdx == index) {
                break;
            }
        }
        return slashMatcher.start();
    }

    public static void main(String[] args) {
//        String source ="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3602.2 Safari/537.36";
        String source = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3602.2 Safari/537.36\" \"-\" skhfdskjfh fhdskfhdskj fsdlkfdsi fhsiofyewio \" \" jfdsujeoiwu" ;
        UserAgentParser userAgentParser = new UserAgentParser();
        UserAgent agent = userAgentParser.parse(source);
        String brower = agent.getBrowser();
        String engine = agent.getEngine();
        String engineVersion = agent.getEngineVersion();
        String os = agent.getOs();
        String platform = agent.getPlatform();
        boolean isMoblie = agent.isMobile();

        System.out.println(brower + "," + engine + "," + engineVersion + "," + os + "," + platform + "," + isMoblie);
    }
}
