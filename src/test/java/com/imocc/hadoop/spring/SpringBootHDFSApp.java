package com.imocc.hadoop.spring;/**
 * Created by Administrator on 2019/2/25 0025.
 */
import org.apache.hadoop.fs.FileStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.hadoop.fs.FsShell;

/**
 * @author ydf
 * @com kt
 * @create 2019-02-25 上午 10:41
 * 使用spring boot的方式访问HDFS
 * https://docs.spring.io/spring-hadoop/docs/2.5.0.RELEASE/reference/html/
 **/
@SpringBootApplication
public class SpringBootHDFSApp implements CommandLineRunner {
    @Autowired
    FsShell fsShell;

    public void run(String... strings) throws Exception {
//        fsShell.mkdir("/fsShell/kk/cc/");
//        fsShell.rmr("/fsShell/");
        for(FileStatus fileStatus: fsShell.lsr("/springhdfs")){
            System.out.println(">"+fileStatus.getPath());
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootHDFSApp.class,args);
    }
}
