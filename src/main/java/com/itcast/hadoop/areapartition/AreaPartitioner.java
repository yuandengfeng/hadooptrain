package com.itcast.hadoop.areapartition;/**
 * Created by Administrator on 2019/4/4 0004.
 */

import org.apache.hadoop.mapreduce.Partitioner;

import java.util.HashMap;

/**
 * @author ydf
 * @com kt
 * @create 2019-04-04 下午 3:20
 **/
public class AreaPartitioner<KEY, VALUE> extends Partitioner<KEY, VALUE> {

    private static HashMap<String, Integer> areaMap = new HashMap<String, Integer>();

    static {
        areaMap.put("135", 0);
        areaMap.put("136", 1);
        areaMap.put("137", 2);
        areaMap.put("138", 3);
        areaMap.put("139", 4);
    }


    @Override
    public int getPartition(KEY key, VALUE value, int i) {
        //从key中拿到手机号，查询手机归属地字典，不同省份返回不同组号
        //其他值返回5
        int areaCoder = areaMap.get(key.toString().substring(0, 3)) == null ? 5 : areaMap.get(key.toString().substring(0, 3));
        return areaCoder;
    }
}
