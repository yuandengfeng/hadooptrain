package com.itcast.hadoop.flowsum;/**
 * Created by Administrator on 2019/4/3 0003.
 */

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author ydf
 * @com kt
 * @create 2019-04-03 下午 3:49
 **/
public class FlowSumMapper extends Mapper<LongWritable,Text,Text,FlowBean> {

    /**
     *     拿到日志中的一行数据，切分抽取需要字段：手机号，上行流量，下行流量，然后封装成kv发送出去
     * 1363157983019   13719199419     68-A1-B7-03-07-B1:CMCC-EASY     120.196.100.82                  4       0       240     0       200
     * 1363157984041   13660577991     5C-0E-8B-92-5C-20:CMCC-EASY     120.197.40.4    s19.cnzz.com    站点统计        24      9       6960    690     200
     * 1363157973098   15013685858     5C-0E-8B-C7-F7-90:CMCC  120.197.40.4    rank.ie.sogou.com       搜索引擎        28      27      3659    3538    200
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line=value.toString();
        String[] fields= StringUtils.split(line,"\t");
        String phoneNB=fields[1];
        long up_flow=Long.parseLong(fields[7]);
        long d_flow=Long.parseLong(fields[8]);

        //封装数据为kv并输出
        context.write(new Text(phoneNB),new FlowBean(phoneNB,up_flow,d_flow));
    }
}
