package com.itcast.hadoop.flowsum;/**
 * Created by Administrator on 2019/4/3 0003.
 */

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author ydf
 * @com kt
 * @create 2019-04-03 下午 3:50
 **/
public class FlowSumReducer extends Reducer<Text,FlowBean,Text,FlowBean> {

    //框架没传递一组数据，调用一次我们的reduce方法
    @Override
    protected void reduce(Text key, Iterable<FlowBean> values, Context context) throws IOException, InterruptedException {

        long up_flow_counter=0;
        long d_flow_counter=0;
        for (FlowBean bean:values){
            up_flow_counter+=bean.getUp_flow();
            d_flow_counter+=bean.getD_flow();
        }
        context.write(key,new FlowBean(key.toString(),up_flow_counter,d_flow_counter));
    }
}

