package com.Test;/**
 * Created by Administrator on 2019/3/8 0008.
 */

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.hive.ql.exec.ByteWritable;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

/**
 * @author ydf
 * @com kt
 * @create 2019-03-08 上午 11:29
 **/
public class A {
    public void t1() {
        System.out.println("t1");
    }

    public void t2() {
        t1();
        System.out.println("t2");
    }

    public void t3() {
        t2();
        System.out.println("t3");
    }

    public static void main(String[] args) throws Exception {
        List<String> list = FileUtils.readLines(new File("C:\\Users\\Administrator\\Downloads\\k1.1555645227748.ok"));

        for (String string : list) {
            System.out.println(string);
            byte[] bytes = string.getBytes("UTF-8");
            System.out.println("bytes-->" + Arrays.toString(bytes));//打印byte数组
            System.out.println("string-->" + new String(bytes));//获得byte数组转换来的String数据，并打印
        }
//        String string = "hello 世界小姐";
//
//        byte[] bytes = string.getBytes("GBK");//获得byte数组
//
//        System.out.println("bytes-->" + Arrays.toString(bytes));//打印byte数组
//
//        System.out.println("string-->" + new String(bytes));//获得byte数组转换来的String数据，并打印

    }
}
