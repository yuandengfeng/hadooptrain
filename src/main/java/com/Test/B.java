package com.Test;/**
 * Created by Administrator on 2019/3/8 0008.
 */

/**
 * @author ydf
 * @com kt
 * @create 2019-03-08 上午 11:31
 **/
public class B extends A {
    @Override
    public void t2() {
        super.t1();
        System.out.println("tb2");
    }

    public static void main(String[] args) {
        A a=new B();
        a.t2();
        a.t3();
    }
}
