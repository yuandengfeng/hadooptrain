package com.hive;
/**
 * Created by Administrator on 2019/4/12 0012.
 */

import org.apache.hadoop.hive.ql.exec.UDF;

import java.util.HashMap;

/**
 * @author ydf
 * @com kt
 * @create 2019-04-12 下午 4:47
 * 源数据
 * 1388990045,http://www.163.com,2000
 * 1388566005,http://www.163.com,2000
 * 1388566005,http://www.163.com,2000
 * 1399990045,http://www.163.com,2000
 * 1399876045,http://www.163.com,2000
 * 1386666005,http://www.163.com,2000
 * 1366876045,http://www.163.com,2000
 * 1366876045,http://www.163.com,2000
 * 1366990045,http://www.163.com,2000
 * 使用：
 * 打成jar包上传至hive服务器
 * 1. hive>add jar /opt/hive/test/hivearea.jar;
 * 2. hive>CREATE TEMPORARY FUNCTION getarea AS 'com.hive.PhoneNbrToArea';
 * 3. hive>create table t_flow(phonebr string,url string,tt int) row format delimited fields terminated by ',';
 * 4. hive>load data local inpath '/opt/hive/test/flow.txt' into table t_flow;
 * 5. hive>select getarea(phonebr) from t_flow;
 **/
public class PhoneNbrToArea extends UDF {
    private static HashMap<String, String> areaMap = new HashMap<String, String>();

    static {
        areaMap.put("1388", "beijing");
        areaMap.put("1399", "tianjin");
        areaMap.put("1366", "nanjing");
    }

    //一定要用public修饰才能被hive调用
    public String evaluate(String pnb) {
        String result = areaMap.get(pnb.substring(0, 4)) == null ? (pnb + "    huoxing") : (pnb + "  " + areaMap.get(pnb.substring(0, 4)));
        return result;
    }

}
